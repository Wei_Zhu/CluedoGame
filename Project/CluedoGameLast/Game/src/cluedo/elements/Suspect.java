package cluedo.elements;

import java.awt.Color;

/**
 * 
 * Class to describe a Suspect in the game.
 * 
 **/
public class Suspect implements GameItem {

    private String name;
	private boolean playable;
	private boolean isSolution;
	private int startCol;
	private int startRow;

	public Suspect(String name, int col, int row) {
		this.name = name;
		this.startCol = col;
		this.startRow = row;
		this.playable = false;
		this.isSolution = false;
	}

	public int[] getStartPos() {
		int[] pos = { startCol, startRow };
		return pos;
	}

	/**
	 * @return the playable
	 */
	public boolean isPlayable() {
		return playable;
	}

	/**
	 * @param playable
	 *            the playable to set
	 */
	public void setPlayable(boolean playable) {
		this.playable = playable;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public void setSolution() {
		isSolution = true;
	}

	public boolean isSolution() {
		return isSolution;
	}

	 public Color getColor() {
	 if (this.name.equals("Miss Scarlett")) {
	 return new Color(255, 0, 0); /** Miss Scarlett is denoted by Red dot**/
	 } else if (this.name.equals("Professor Plum")) {
	 return new Color(148, 0, 211); /** Professor Plum is denoted by Purple dot**/
	 } else if (this.name.equals("Mrs Peacock")) {
	 return new Color(0, 255, 255); /** Mrs Peacock is denoted by Cyan dot**/
	 } else if (this.name.equals("Reverend Mr Green ")) {
	 return new Color(0,128,0); /** Mr Green is denoted by Green dot**/
	 } else if (this.name.equals("Colonel Mustard")) {
	 return new Color(255, 255, 0); /** Colonel Mustard is denoted by Yellow
	 dot**/
	 } else if (this.name.equals("Mrs White")) {
	 return new Color(255, 255, 255); /** Mrs White is denoted by a White dot**/
	 }
	
	 throw new Error("Incorrect Name");
	 }

}
