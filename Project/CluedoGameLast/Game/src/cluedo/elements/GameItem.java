/**
 * 
 */
package cluedo.elements;

/**
 * @author zhuwe
 *
 */
public interface GameItem {

	/**
	 * Returns the name of this object
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * Returns whether or not this item is in the clue.
	 * 
	 * @return
	 */
	public boolean isSolution();

	/**
	 * Sets whether or not this item is one of the murder items.
	 */
	public void setSolution();

}
