package cluedo.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhuwe
 *
 */
public class NoteBook {
	private Map<GameItem, Boolean> notebook;
	private List<Room> rooms;
	private List<Weapon> weapons;
	private List<Suspect> gSuspects;

	public NoteBook() {
		notebook = new HashMap<GameItem, Boolean>();
		rooms = new ArrayList<Room>();
		weapons = new ArrayList<Weapon>();
		gSuspects = new ArrayList<Suspect>();
	}

	/**
	 * A method to add or change the value for a Card.
	 * 
	 * @param the Card
	 *            
	 * @param it's truth value
	 *           
	 * 
	 */
	public void addToNoteBook(GameItem gi, boolean bool) {
		notebook.put(gi, bool);
		if (gi instanceof Room && !rooms.contains(gi)) {
			rooms.add((Room) gi);
		} else if (gi instanceof Weapon && !weapons.contains(gi)) {
			weapons.add((Weapon) gi);
		} else if (gi instanceof Suspect && !gSuspects.contains(gi)) {
			gSuspects.add((Suspect) gi);
		}
	}

	/**
	 * A method that returns the truth value of a given Card.
	 * 
	 * @param the Card
	 *            
	 * @return it's truth value
	 */
	public boolean getFromNoteBook(GameItem gi) {
		return notebook.get(gi);
	}

	public Map<GameItem, Boolean> getBook() {
		return notebook;
	}

	/**
	 * @return the rooms
	 */
	public List<Room> getRooms() {
		return rooms;
	}

	/**
	 * @return the weapons
	 */
	public List<Weapon> getWeapons() {
		return weapons;
	}

	/**
	 * @return the gCharacters
	 */
	public List<Suspect> getSuspects() {
		return gSuspects;
	}

}
