package cluedo.elements;

public class Weapon implements GameItem {

	private Weapons name;
	private boolean isSolution;

	public Weapon(Weapons name) {
		this.name = name;
		this.isSolution = false;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name.toString();
	}

	@Override
	public boolean isSolution() {
		// TODO Auto-generated method stub
		return isSolution;
	}

	@Override
	public void setSolution() {
		// TODO Auto-generated method stub
		isSolution = true;
	}
	
	public enum Weapons {
		Rope, Candlestick, Knife, LeadPipe, Revolver, Poison
	}

}
