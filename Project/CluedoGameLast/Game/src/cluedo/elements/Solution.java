package cluedo.elements;

/**
 * this class for holding the answers to the murder.

 *
 */
public class Solution {

    private Weapon w;
    private Room r;
    private Suspect s;

    public Solution(Weapon w, Room r, Suspect s){
        this.w = w;
        this.r = r;
        this.s = s;
    }

    public Weapon getWeapon(){
        return w;
    }

    public Room getRoom(){
        return r;
    }

    public Suspect getSuspect(){
        return s;
    }

    public boolean accuse(GameItem g){
        if(g instanceof Weapon){
            return g == w;
        } else if(g instanceof Room){
            return g == r;
        } else if(g instanceof Suspect){
            return g == s;
        } else {
            throw new Error("Invalid Data!!");
        }
    }

}
