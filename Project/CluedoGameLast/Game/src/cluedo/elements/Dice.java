package cluedo.elements;

import java.util.Random;

/**
Random number generator to simulate a dice. Generates a random number between 1 and 6
 
 **/
public class Dice {

    Random rand = new Random();

    /**
     * Returns a random integer between 2 and 12.
     * 
     */
    public int roll() {
        int dice1 = rand.nextInt(6) + 1;
        int dice2 = rand.nextInt(6) + 1;
        return dice1 + dice2;
    }
}
