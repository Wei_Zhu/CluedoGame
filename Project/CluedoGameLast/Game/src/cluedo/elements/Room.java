package cluedo.elements;

import java.util.ArrayList;
import java.util.List;

import cluedo.slots.RoomSquare;

/**
 * @author zhuwe
 *
 */
public class Room implements GameItem {

	private String name;
	private List<Weapon> weapons;
	private List<Suspect> playersIn;
	private boolean isSolution;

	private RoomSquare rs;

	public Room(String name) {
		this.name = name;
		this.weapons = new ArrayList<Weapon>();
		this.playersIn = new ArrayList<Suspect>();
		this.isSolution = false;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public boolean isSolution() {
		// TODO Auto-generated method stub
		return isSolution;
	}

	public void setSolution() {
		// TODO Auto-generated method stub
		isSolution = true;
	}

	/**
	 * @return the weapon
	 */
	public List<Weapon> getWeapons() {
		return weapons;
	}

	/**
	 * @param weapon
	 *            the weapon to set
	 */
	public void addWeapon(Weapon weapon) {
		this.weapons.add(weapon);
	}

	public void removeWeapon(Weapon w) {
		this.weapons.remove(w);
	}

	/**
	 * @param the
	 *            Game Suspect to enter the room
	 */
	public void enterRoom(Suspect suspect) {
		playersIn.add(suspect);
	}

	/**
	 * @param the
	 *            Game Suspect for checking
	 * @return true / false
	 */
	public boolean isInRoom(Suspect suspect) {
		return playersIn.contains(suspect);
	}

	/**
	 * @param the
	 *            Game Suspect to leave the room
	 */
	public void leaveRoom(Suspect suspect) {
		playersIn.remove(suspect);
	}

	/**
	 * @return if the room is empty or not
	 */
	public boolean isEmpty() {
		return playersIn.size() == 0;
	}

	/**
	 * Give the room a pointer to it's RoomSquare
	 * 
	 * @param rs
	 */
	public void setRoomSquare(RoomSquare rs) {
		this.rs = rs;
	}

	public RoomSquare getRoomSquare() {
		return rs;
	}

	public Suspect getSuspectInRoom() {
		return playersIn.get(0);
	}
	
	
}
