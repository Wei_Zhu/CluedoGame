package cluedo.cards;


import cluedo.elements.GameItem;
import cluedo.elements.Room;
import cluedo.elements.Suspect;
import cluedo.elements.Weapon;

/**
 * @author zhuwe
 * 
 * Class that represents an item in the game. It holds two string
 * values, the type of card it is (weapon, room, suspect) and the
 * name of the card. It also holds the GameItem object that it
 * represents.
 *
 */
public class Card {
	
	private String type;
    private String name;
    private GameItem gi;
    
    public Card(GameItem gi) {
        this.type = configureType(gi);
        this.gi = gi;
    }
    private String configureType(GameItem gi) {
        if (gi instanceof Weapon) {
            this.name = gi.getName();
            return "Weapon";
        } else if (gi instanceof Room) {
            this.name = gi.getName();
            return "Room";
        } else if (gi instanceof Suspect) {
            this.name = gi.getName();
            return "Suspect";
        } else {
            throw new Error("GameItem type was wrong? This shouldn't have happened");
        }
    }

    public GameItem getItem(){
        return gi;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

}
