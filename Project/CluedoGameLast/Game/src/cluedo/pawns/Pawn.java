package cluedo.pawns;

import cluedo.elements.GameItem;
import cluedo.elements.Suspect;
import cluedo.elements.Weapon;

/**
 * 
 * @author zhuwe
 * 
 * Class that represents an item in the game. It holds two string
 * values, the type of pawn it is (weapon, suspect) and the
 * name of the Pawn. It also holds the GameItem object that it
 * represents.
 *
 */

public class Pawn {
	
	private String type;
    private String name;
    private GameItem gi;
    
    public Pawn(GameItem gi) {
        this.type = configureType(gi);
        this.gi = gi;
    }
    
    private String configureType(GameItem gi) {
        if (gi instanceof Weapon) {
            this.name = gi.getName();
            return "Weapon";
        } else if (gi instanceof Suspect) {
            this.name = gi.getName();
            return "Suspect";
        } else {
            throw new Error("GameItem type was wrong? This shouldn't have happened");
        }
    }

    public GameItem getItem(){
        return gi;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

}
