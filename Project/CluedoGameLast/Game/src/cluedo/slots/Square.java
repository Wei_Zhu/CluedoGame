package cluedo.slots;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import cluedo.game.Player;

public abstract class Square {

	protected int row;
	protected int col;
	protected Square pathTo;
	protected int pathLength;
	protected Player player;
	protected int xPos;
	protected int yPos;
	protected int width;
	protected int height;
	protected boolean selectable;
	private List<Rectangle> squares = new ArrayList<Rectangle>();

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public void setSize(int x, int y) {
		width = x;
		height = y;

		xPos = row * x;
		yPos = col * y;
	}

	public void draw(Graphics g) {
		if (selectable) {
			g.setColor(new Color(127, 255, 0));
			g.drawRect(xPos, yPos, width, height);
			g.drawRect(xPos + 1, yPos + 1, width - 2, height - 2);
		}
		if (player != null) {
			if (player.isCurrent()) {
				g.setColor(new Color(127, 255, 0));
				g.fillOval(xPos, yPos, width, height);
			}
			Color col = player.getSuspect().getColor();
			g.setColor(col);

			g.fillOval(xPos + 2, yPos + 2, width - 4, height - 4);

			g.setColor(Color.BLACK);
			g.drawOval(xPos + 2, yPos + 2, width - 4, height - 4);
		}
	}

	public Rectangle2D getBoundingBox() {
		return new Rectangle(xPos, yPos, width, height);
	}

	public void setPlayer(Player p) {
		this.player = p;
	}

	public Player getPlayer() {
		return player;
	}

	public void setSelectable(boolean b) {
		this.selectable = b;
	}

}
