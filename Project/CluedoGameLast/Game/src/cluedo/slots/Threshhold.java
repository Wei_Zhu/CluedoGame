package cluedo.slots;

import cluedo.elements.Room;

/**
 * Represents an entrance to a room.
 * @author Hunter
 *
 */
public class Threshhold extends Square{

    private Room r;

    public Threshhold(Room r, int col, int row){
        this.r = r;
        super.col = col;
        super.row = row;
    }

    public Room getRoom(){
        return r;
    }


}
