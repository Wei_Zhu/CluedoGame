package cluedo.slots;

/**
 * Ordinary square in the hallway of the building.
 * @author Hunter
 *
 */
public class FreeMove extends Square{

    public FreeMove(int col, int row){
        super.col = col;
        super.row = row;
    }

}
