package cluedo.slots;

import java.util.ArrayList;
import java.util.List;

import cluedo.elements.Room;
import cluedo.game.Player;


/**
 * This square contains methods for adding players to the room that it
 * represents.
 * 
 * @author Hunter
 *
 */
public class RoomSquare extends Square {

	private Room r;
	private List<Player> playersOn;
	private List<Null> nulls;
	private int col;
	private int row;
	

	public RoomSquare(Room r, int col, int row) {
		this.r = r;
		this.col = col;
		this.row = row;
		playersOn = new ArrayList<Player>(6);
		nulls = new ArrayList<Null>(8);
	}

	public void setNull(Null ns) {
		nulls.add(ns);
	}

	public List<Null> getNulls() {
		return nulls;
	}

	public Room getRoom() {
		return r;
	}

	public void addToRoom(Player p, Square previous) {
		playersOn.add(p);
		for (Null ns : nulls) {
			if (ns.getPlayer() == null) {
				ns.setPlayer(p);
//				p.setOn(ns);
				break;
			}
		}
		previous.setPlayer(null);
	}

	public void leaveRoom(Player p) {
		playersOn.remove(p);

		for (Null ns : nulls) {
			if (ns.getPlayer() == p) {
				ns.setPlayer(null);
				break;
			}
		}
	}

	public List<Player> getPlayersOn() {
		return playersOn;
	}
	
	public int getCol() {
		return col;
		
	}
	
	public int getRow() {
		return row;
		
	}

}
