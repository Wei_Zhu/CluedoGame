package cluedo.slots;

/**
 * Players cannot move through these squares.
 * @author Hunter
 *
 */
public class Wall extends Square{

    public Wall(int col, int row){
        super.col = col;
        super.row = row;
    }

}
