package cluedo.slots;

/**
 * The Squares that the players start on.
 * @author Hunter
 *
 */
public class StartSquare extends Square {

    public StartSquare(int col, int row) {
        super.col = col;
        super.row = row;
    }

}
