package cluedo.slots;

import cluedo.elements.Room;

public class Door extends Square{

    private Room r;

    public Door(Room r, int col, int row){
        this.r = r;
        super.col = col;
        super.row = row;
    }

    /**
     * @return the room
     */
    public Room getR() {
        return r;
    }

}
