/**
 * 
 */
package cluedo.game;

import java.util.ArrayList;
import java.util.List;

import cluedo.cards.Card;
import cluedo.elements.Solution;

/**
 * 
 * Test to see that classes are working properly
 * 
 * @author zhuwe
 *
 */

public class TestClasses {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/* Test the function of show board */
		Board board = new Board();
		System.out.println("------------------------------------------------------------");
		System.out.println("------------------------------------------------------------");
		
		/* Test the function of game Initialization */
		Game game = new Game(board);
		
		/* Test the function of createDummyPlayers   */
		System.out.println("Test the function of createDummyPlayers");
		System.out.println("------------------------------------------------------------");
		for(Player p : game.getPlayers()) {
			System.out.println("we have player: "+ p.getSuspect().getName());
		}
		System.out.println("------------------------------------------------------------");

		/* Test the function of set start squares   */
		System.out.println("Test the function of set start squares");
		System.out.println("------------------------------------------------------------");
		game.setStartSquares();
		for(Player p : game.getPlayers()) {
			System.out.println(p.getSuspect().getName()+" start at position (row, col): "+p.getRow()+", "+p.getCol() );
		}
		System.out.println("------------------------------------------------------------");
		
		
		game.createCardsAndSolution();
		game.distributeCards();
		/* Test the function of Create Card  and Distribute Card*/
		System.out.println("Test the function of Create Card  and Distribute Card");
		System.out.println("------------------------------------------------------------");
		List<Card> totalCard = new ArrayList<Card>();
		List<Card> playercard = new ArrayList<Card>();
		for(Player p : game.getPlayers()) {
			playercard = p.getCard();
			for(Card card: playercard) {
				totalCard.add(card);
				System.out.println("player "+p.getSuspect().getName()+ " has Card: "+ card.getName());
			}
			
		}
		System.out.println("------------------------------------------------------------");
		
		
		/* Test the function of Solution */
		System.out.println("Test the function of Solution");
		System.out.println("------------------------------------------------------------");
		Solution solution = game.getSolution();
		System.out.println("solution of Suspect: " + solution.getSuspect().getName());
		System.out.println("solution of Room: " + solution.getRoom().getName());
		System.out.println("Solution of Weapon: " + solution.getWeapon().getName());
		System.out.println("------------------------------------------------------------");

		
		/* Test the function of Create Notebook */
		System.out.println("Test the function of Create Notebook");
		System.out.println("------------------------------------------------------------");
		for(Player p : game.getPlayers()) {
			System.out.println(p.getSuspect().getName()+"'s notebook: ");
		}
		System.out.println("------------------------------------------------------------");

		
	
		/* Now play the game, check the game running, player movement, accuse and hypothesis*/
		System.out.println("------------------------------------------------------------");
		game.loopGame();
		System.out.println("------------------------------------------------------------");
		
		
		System.out.print("******* End of the program *********");

	}
}
