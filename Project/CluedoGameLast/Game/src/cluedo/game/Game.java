package cluedo.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Scanner;

import cluedo.cards.Card;
import cluedo.elements.Room;
import cluedo.elements.Solution;
import cluedo.elements.Suspect;
import cluedo.elements.Weapon;
import cluedo.system.ChoosePlayer;

public class Game {

	private Board board;
	private List<Player> players;
	private Solution solution;
	private Queue<Card> rumorDeck;
	private int currentPointer;
	private Player current;
	private Movement movement;
	private Room accuseRoom;

	public Game(Board board) {
		this.board = board;
		players = new ArrayList<Player>();
		rumorDeck = new LinkedList<Card>();
		movement = new Movement();
		new ChoosePlayer(this);
	}

	/**
	 * Initializes the game, creates all the objects that are needed and sets the
	 * current player.
	 */
	public void playGame() {
		setStartSquares();
		createCardsAndSolution();
		distributeCards();
		createNoteBooks();
		loopGame();
	}

	public void loopGame() {
		currentPointer = 0;
		while (currentPointer < players.size()) {
			current = players.get(currentPointer);
			if (current.isPlaying()) {
				current.setCurrent(true);
				if (current.getInRoom()) {
					System.out.println("--------------------------------------------------------------------------");
					System.out.println("you want to accuse or hypothesis:");
					System.out.println("if you want to accuse, please type A");
					System.out.println("if you want to hypothesis, please type H");
					System.out.println("if you want to continue to move, please type M");
					System.out.println("--------------------------------------------------------------------------");
					Scanner choice = new Scanner(System.in);
					String line = choice.nextLine();
					char first = line.charAt(0);
					switch (first) {

					case 'A':
						accuse();
						break;

					case 'H':
						hypothesis();
						current.setCurrent(false);
						currentPointer++;
						break;
					case 'M':
						movement.playerMove(current, board);
						current.setCurrent(false);
						current.setInRoom(false);
						currentPointer++;
						break;

					default:
						break;

					}
				} else if (movement.isIndiagonalslot(current)) {
					movement.diagonalslot(current, board);
					current.setCurrent(false);
					currentPointer++;
				} else {
					movement.playerMove(current, board);
					current.setCurrent(false);
					currentPointer++;
				}

			}
			if (currentPointer == players.size()) {
				currentPointer = 0;
			}
		}
	}

	public void createDummyPlayers(int charNum) {
		Player player = new Player(board.getSuspects().get(charNum - 1), board, this);

		player.setPlaying(true);
		board.getSuspects().get(charNum - 1).setPlayable(true);
		players.add(player);

	}

	// public void createDummyPlayers() {
	// for (Suspect sp : board.getSuspects()) {
	// if (sp.isPlayable()) {
	// Player dummy = new Player(sp, board, this);
	// players.add(dummy);
	// }
	// }
	// }

	// method for test
	public List<Player> getPlayers() {
		return players;

	}

	// method for test
	public Solution getSolution() {
		return solution;

	}

	/**
	 * Puts the players onto the specific squares they are supposed to start on,
	 * defined by the rules of the game.
	 */
	public void setStartSquares() {
		for (Player p : players) {
			if (p.getSuspect().getName().equals("Miss Scarlett")) {
				p.setOn(board.getSquare(28, 18));
				board.getSquare(28, 18).setPlayer(p);
				p.setRow(28);
				p.setCol(18);
			} else if (p.getSuspect().getName().equals("Colonel Mustard")) {
				p.setOn(board.getSquare(28, 7));
				board.getSquare(28, 7).setPlayer(p);
				p.setRow(28);
				p.setCol(7);
			} else if (p.getSuspect().getName().equals("Mrs White")) {
				p.setOn(board.getSquare(0, 20));
				board.getSquare(0, 20).setPlayer(p);
				p.setRow(0);
				p.setCol(20);
			} else if (p.getSuspect().getName().equals("Mr Green")) {
				p.setOn(board.getSquare(9, 0));
				board.getSquare(9, 0).setPlayer(p);
				p.setRow(9);
				p.setCol(0);
			} else if (p.getSuspect().getName().equals("Mrs Peacock")) {
				p.setOn(board.getSquare(0, 6));
				board.getSquare(0, 6).setPlayer(p);
				p.setRow(0);
				p.setCol(6);
			} else if (p.getSuspect().getName().equals("Professor Plum")) {
				p.setOn(board.getSquare(19, 0));
				board.getSquare(19, 0).setPlayer(p);
				p.setRow(19);
				p.setCol(0);
			}
		}
	}

	/**
	 * Creates a queue of cards in a random order. Randomly creates the Clue object.
	 *
	 */
	public void createCardsAndSolution() {
		Random rand = new Random();

		Weapon solutionWep = board.getWeapons().get(rand.nextInt(6));

		Room solutionRoom = board.getRooms().get(rand.nextInt(9));

		Suspect solutionSp = board.getSuspects().get(rand.nextInt(6));

		solutionWep.setSolution();
		solutionRoom.setSolution();
		solutionSp.setSolution();
		solution = new Solution(solutionWep, solutionRoom, solutionSp);

		List<Card> temp = new ArrayList<Card>(21);

		for (Suspect sp : board.getSuspects()) {
			if (sp.isSolution()) {
				continue;
			}
			temp.add(new Card(sp));
		}

		for (Room r : board.getRooms()) {
			if (r.isSolution()) {
				continue;
			}
			temp.add(new Card(r));
		}

		for (Weapon w : board.getWeapons()) {
			if (w.isSolution()) {
				continue;
			}
			temp.add(new Card(w));
		}

		// Randomly takes items out the temp list and offers them to the queue
		// Ie simulates a shuffled deck of rumor cards.
		Collections.shuffle(temp);

		for (Card c : temp) {
			rumorDeck.offer(c);
		}

	}

	/**
	 * "Deals" the rumor cards to the players.
	 */
	public void distributeCards() {
		while (!rumorDeck.isEmpty()) {
			for (Player p : players) {
				if (p.isPlaying()) {
					if(rumorDeck.isEmpty()) {
						break;
					}else {
						p.addCard(rumorDeck.poll());
					}
					
				}
			}

		}
	}

	/**
	 * Gives each player a note book, with every value initially false. Then updates
	 * the note books.
	 */
	public void createNoteBooks() {
		for (Suspect sp : board.getSuspects()) {
			for (Player p : players) {
				p.updateNoteBook(sp, false);
			}

		}
		for (Room r : board.getRooms()) {
			for (Player p : players) {
				p.updateNoteBook(r, false);
			}

		}

		for (Weapon w : board.getWeapons()) {
			for (Player p : players) {
				p.updateNoteBook(w, false);
			}

		}

		// Updates the notebooks depending on which cards the player has.
		for (Player p : players) {
			p.updateNoteBook();
		}
	}

	public static void main(String[] args) {
		new Game(new Board());
	}

	public void hypothesis() {

		System.out.println("current player in Room: " + current.getRoomName());
		System.out.println("your hypothese room has to be same as the room you are in now !!!");

		System.out.println("Please enter Hypothesis Room name:");
		Scanner scan3 = new Scanner(System.in);
		String hyporoomin = scan3.nextLine();

		System.out.println("Please enter Hypothesis Weapon name:");
		Scanner scan1 = new Scanner(System.in);
		String hypoweaponin = scan1.nextLine();

		System.out.println("Please enter Hypothesis Suspect name:");
		Scanner scan2 = new Scanner(System.in);
		String line = scan2.nextLine();
		Scanner lineScan = new Scanner(line);
		String hyposuspectin = lineScan.next() + " " + lineScan.next();

		List<Card> HypothesesPlayerCard = new ArrayList<Card>();
		boolean flag = true;
		if (hyporoomin.equals(current.getRoomName())) {

			for (int i = currentPointer; i > 0; i--) {
				HypothesesPlayerCard = (players.get(i - 1).getCard());
				for (Card playercard : HypothesesPlayerCard) {
					if (playercard.getName().equals(hypoweaponin) || playercard.getName().equals(hyposuspectin)
							|| playercard.getName().equals(current.getRoomName())) {
						System.out.println("the hypotheses was refused by previous "+i+" player before current player");
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				for (int j = players.size(); j > currentPointer; j--) {
					HypothesesPlayerCard = (players.get(j - 1).getCard());
					for (Card playercard : HypothesesPlayerCard) {
						if (playercard.getName().equals(hypoweaponin) || playercard.getName().equals(hyposuspectin)
								|| playercard.getName().equals(current.getRoomName())) {
							System.out.println("the hypotheses was refused by previous "+j+" player before current player");
							break;
						} 
					}

				}
			}

		} else {
			System.out.println("your hypothese room has to be same as the room you are in now");
		}

	}

	public void accuse() {

		System.out.println("Please enter Accused Room name:");
		Scanner scan = new Scanner(System.in);
		String accuseroomin = scan.nextLine();

		System.out.println("Please enter Accused Weapon name:");
		Scanner scan1 = new Scanner(System.in);
		String accuseweaponin = scan.nextLine();

		System.out.println("Please enter Accused Suspect name:");
		Scanner scan2 = new Scanner(System.in);
		String line = scan2.nextLine();
		Scanner lineScan = new Scanner(line);
		String accusesuspectin = lineScan.next() + " " + lineScan.next();

		for (Room room : board.getRooms()) {
			if (room.getName().equals(accuseroomin)) {
				accuseRoom = room;
			}
		}

		current.setCol(accuseRoom.getRoomSquare().getCol());
		current.setRow(accuseRoom.getRoomSquare().getRow());
		System.out.println("this is the new position of " + current.getSuspect().getName() + "(" + current.getCol()
				+ ", " + current.getRow() + ")");

		// set weapon position in the room accusser room in

		if (accuseweaponin.equals(solution.getWeapon().getName())
				&& accusesuspectin.equals(solution.getSuspect().getName())
				&& accuseroomin.equals(solution.getRoom().getName())) {
			System.out.println("Current Player Wins");
			System.out.println("Game Over");
			System.exit(0);
		} else {

			System.out.println("You accused wrongly hence you loose and have to leave the game");
			current.setPlaying(false);
			players.remove(currentPointer);
			current.setCurrent(false);
		}

		currentPointer++;
	}

}
