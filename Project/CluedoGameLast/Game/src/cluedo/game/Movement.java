package cluedo.game;

import java.util.Scanner;

import cluedo.slots.Square;
import cluedo.slots.Wall;

public class Movement {

	private int type;

	public Movement() {
		// TODO Auto-generated constructor stub
		this.type = 0;
	}
	//Movement
	// While The movement can be Up or down or left or Right.
	//Hence an example would be U1L2 which means Up by one and left by two.
	// We check for each step and perform the movement in sections ie We move Down or up first then we move left or right.
	// If during the movement the player hits the wall we step back to the last position
	public void playerMove(Player current, Board board) {
		int rollnum = current.roll();
		System.out.println("Player " + current.getSuspect().getName() + " " + "roll dice, get number: " + rollnum);
		System.out.println(
				"Please enter Row direction and distance & Column direction and distance. ex. U2L2 or U2R2 or D2L2 or D2R2");
		Scanner scan = new Scanner(System.in);
		String line = scan.nextLine();
		char first = line.charAt(0);
		char third = line.charAt(2);
		int stepRow = line.charAt(1) - '0';
		int stepColumn = line.charAt(3) - '0';

		boolean meetWall = false;
		int playerRow = current.getRow();
		int playerColumn = current.getCol();

		switch (first) {
		case 'U':
			while (stepRow > 0) {
				playerRow = playerRow - 1;
				Square s = board.getSquare(playerColumn, playerRow);

				if (s instanceof Wall) {
					System.out.println("Invalid Move. You will hit the wall in vertical direction");
					meetWall = true;
					break;
				}
				stepRow--;
			}
			if (!meetWall) {
				current.setRow(current.getRow() - (line.charAt(1) - '0'));
				System.out.println("After vertical move, " + "player " + current.getSuspect().getName()
						+ "at position: " + current.getRow() + "," + current.getCol());
			}

			break;
		case 'D':
			while (stepRow > 0) {
				playerRow = playerRow + 1;
				Square s = board.getSquare(playerColumn, playerRow);

				if (s instanceof Wall) {
					System.out.println("Invalid Move. You will hit the wall in vertical direction");
					meetWall = true;
					break;
				}
				stepRow--;
			}
			if (!meetWall) {
				current.setRow(current.getRow() + (line.charAt(1) - '0'));
				System.out.println("After vertical move, " + "player " + current.getSuspect().getName()
						+ "at position: " + current.getRow() + "," + current.getCol());
			}
			break;
		default:
			break;
		}

		switch (third) {
		case 'L':
			while (stepColumn > 0) {
				playerColumn = playerColumn - 1;
				Square s = board.getSquare(playerColumn, playerRow);

				if (s instanceof Wall) {
					System.out.println("Invalid Move. You will hit the wall in horizontal direction");
					meetWall = true;
					break;
				}
				stepColumn--;
			}
			if (!meetWall) {
				current.setCol(current.getCol() - (line.charAt(3) - '0'));
				System.out.println("After horizontal move, " + "player " + current.getSuspect().getName()
						+ "at position: " + current.getRow() + "," + current.getCol());
				System.out.println("---------------------------------------------------------------");
			}
			roomCheck(current);
			System.out.println("this is the new position of " + current.getSuspect().getName() + " (row, col): "+ "(" + current.getRow()
					+ ", " + current.getCol() + ")");
			break;
		case 'R':
			while (stepColumn > 0) {
				playerColumn = playerColumn + 1;
				Square s = board.getSquare(playerColumn, playerRow);

				if (s instanceof Wall) {
					System.out.println("Invalid Move. You will hit the wall in horizontal direction");
					meetWall = true;
					break;
				}
				stepColumn--;
			}
			// Sets the position of the player to the slot if the move is valid
			if (!meetWall) {
				current.setCol(current.getCol() + (line.charAt(3) - '0'));
				System.out.println("After horizontal move, " + "player " + current.getSuspect().getName()
						+ "at position: " + current.getRow() + "," + current.getCol());
				System.out.println("-------------------------------------------------------------------");
			}
			roomCheck(current);
			System.out.println("this is the new position of " + current.getSuspect().getName() + " (row, col): "+ "(" + current.getRow()
			+ ", " + current.getCol() + ")");
			break;
		default:
			break;

		}
	}

	// Sets the player to a fixed spot inside a room( denoted by R) if the player
	// enters the room
	public void roomCheck(Player current) {
		if (current.getRow() < 7 && current.getRow() > 0 && current.getCol() < 5 && current.getCol() > 0) {
			current.setRow(3);
			current.setCol(2);
			current.setInRoom(true);
			// current.setroom : Conservatory
		}

		else if (current.getRow() < 7 && current.getRow() > 0 && current.getCol() > 8 && current.getCol() < 12) {
			current.setRow(3);
			current.setCol(10);
			current.setInRoom(true);
			// current.setroom : Billiard
		}

		else if (current.getRow() < 7 && current.getRow() > 0 && current.getCol() > 14 && current.getCol() < 19) {
			current.setRow(3);
			current.setCol(10);
			current.setInRoom(true);
			// current.setroom : Library

		}

		else if (current.getRow() < 7 && current.getRow() > 0 && current.getCol() > 22 && current.getCol() < 28) {
			current.setRow(3);
			current.setCol(10);
			current.setInRoom(true);
			// current.setroom : study

		}

		else if (current.getCol() < 7 && current.getCol() > 0 && current.getRow() > 10 && current.getRow() < 18) {
			current.setRow(14);
			current.setCol(2);
			current.setInRoom(true);
			// current.setroom :Ballroom
		}

		else if (current.getCol() < 28 && current.getCol() > 19 && current.getRow() > 12 && current.getRow() < 17) {
			current.setRow(14);
			current.setCol(21);
			current.setInRoom(true);
			// current.setroom : Hall

		}

		else if (current.getRow() < 28 && current.getRow() > 21 && current.getCol() < 6 && current.getCol() > 0) {
			current.setRow(25);
			current.setCol(2);
			current.setInRoom(true);
			// current.setroom :Kitchen
		}

		else if (current.getRow() < 28 && current.getRow() > 19 && current.getCol() > 9 && current.getCol() < 16) {
			current.setRow(25);
			current.setCol(12);
			current.setInRoom(true);
			// current.setroom : Dining Room
		}

		else if (current.getRow() < 28 && current.getRow() > 20 && current.getCol() > 20 && current.getCol() < 28) {
			current.setRow(24);
			current.setCol(24);
			current.setInRoom(true);
			// current.setroom :Lounge
		}
	}

	
	/// We check if the player is on the diagonal slots. 
	// If he is on the diagonal slot then we ask if he want to move across or move into the room etc..
		// Also we set the Boolean inDiagonalSlot to true
	public boolean isIndiagonalslot(Player current) {
		if (current.getRow() == 0 && current.getCol() <= 1) {
			type = 1;
			return true;

		} else if (current.getCol() == 0 && current.getRow() <= 1) {
			type = 1;
			return true;
		}

		// Diagonal Slot 2 at top right corner
		else if (current.getRow() == 0 && current.getCol() >= 27 && current.getCol() <= 28) {
			type = 2;
			return true;
		} else if (current.getCol() == 28 && current.getRow() >= 0 && current.getRow() <= 1) {
			type = 2;
			return true;
		}

		// Diagonal Slot 3 at bottom right corner
		else if (current.getRow() == 28 && current.getCol() >= 27 && current.getCol() <= 28) {
			type = 3;
			return true;
		} else if (current.getCol() == 28 && current.getRow() >= 27 && current.getRow() <= 28) {
			type = 3;
			return true;
		}

		// Diagonal Slot 4 at bottom left corner
		else if (current.getRow() == 28 && current.getCol() >= 0 && current.getCol() <= 1) {
			type = 4;
			return true;
		} else if (current.getCol() == 0 && current.getRow() >= 27 && current.getRow() <= 28) {
			type = 4;
			return true;
		} else {
			return false;
		}
	}

	public void diagonalslot(Player current, Board board) {
		// Checks if the player is at a diagonal slot or not.
		// If he is then asks him to move to the opposite diagonal
		// and sets the position at the opposite diagonal

		// Diagonal Slot 1 at top left corner

		Scanner scan = new Scanner(System.in);

		switch (type) {
		case 1:

			System.out.println("Do you want to go to the opposite diagonal Y or N?\n");
			String r = scan.next();
			if (r.charAt(0) == 'Y') {
				System.out.println("Transporting you to the other side now!!");
				current.setRow(28);
				current.setCol(28);
				System.out.println("Transport complete!!");
				System.out.println("this is the new position of " + current.getSuspect().getName() + "("
						+ current.getCol() + ", " + current.getRow() + ")");

			} else {
				System.out.println("not transport back,chose come to the room");
				playerMove(current, board);
				current.setInRoom(true);
			}
			break;

		case 2:
			System.out.println("Do you want to go to the opposite diagonal Y or N?\n");
			String r1 = scan.next();
			if (r1.charAt(0) == 'Y') {
				System.out.println("Transporting you to the other side now!!");
				current.setRow(28);
				current.setCol(0);
				System.out.println("Transport complete!!");
				System.out.println("this is the new position of " + current.getSuspect().getName() + "("
						+ current.getCol() + ", " + current.getRow() + ")");
			} else {
				System.out.println("not transport back,chose come to the room");
				playerMove(current, board);
				current.setInRoom(true);
			}
			break;

		case 3:
			System.out.println("Do you want to go to the opposite diagonal Y or N?\n");
			String r2 = scan.next();
			if (r2.charAt(0) == 'Y') {
				System.out.println("Transporting you to the other side now!!");
				current.setRow(0);
				current.setCol(0);
				System.out.println("Transport complete!!");
				System.out.println("this is the new position of " + current.getSuspect().getName() + "("
						+ current.getCol() + ", " + current.getRow() + ")");
			} else {
				System.out.println("not transport back,chose come to the room");
				playerMove(current, board);
				current.setInRoom(true);
			}

			break;

		case 4:
			System.out.println("Do you want to go to the opposite diagonal Y or N?\n");
			String r3 = scan.next();
			if (r3.charAt(0) == 'Y') {
				System.out.println("Transporting you to the other side now!!");
				current.setRow(0);
				current.setCol(28);
				System.out.println("Transport complete!!");
				System.out.println("this is the new position of " + current.getSuspect().getName() + "("
						+ current.getCol() + ", " + current.getRow() + ")");
			} else {
				System.out.println("not transport back,chose come into the room");
				playerMove(current, board);
				current.setInRoom(true);
			}
			break;
		default:
			break;
		}

	}

}
