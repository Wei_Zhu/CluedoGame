package cluedo.game;

import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import cluedo.elements.Dice;
import cluedo.elements.Room;
import cluedo.elements.Suspect;
import cluedo.elements.Weapon;
import cluedo.elements.Weapon.Weapons;
import cluedo.slots.Door;
import cluedo.slots.FreeMove;
import cluedo.slots.Null;
import cluedo.slots.RoomSquare;
import cluedo.slots.Square;
import cluedo.slots.StartSquare;
import cluedo.slots.Threshhold;
import cluedo.slots.Wall;

/**
 * 
 * @author zhuwe
 *
 */
public class Board {
	private Square[][] board;
	private List<Room> rooms;
	private List<Suspect> suspects;
	private List<Weapon> weapons;
	private List<Threshhold> threshholds;
	private List<RoomSquare> rSquares;
	public static Dice dice = new Dice();
	private File file;

	public Board() {
		board = new Square[29][29];
		rooms = new ArrayList<Room>();
		suspects = new ArrayList<Suspect>();
		weapons = new ArrayList<Weapon>(9);
		threshholds = new ArrayList<Threshhold>();
		rSquares = new ArrayList<RoomSquare>();
		file = new File("Game/src/Board");
		readAndShowBoard();
	}

	public Square getSquare(int col, int row) {
		return board[row][col];
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public List<Suspect> getSuspects() {
		return suspects;
	}

	public List<Weapon> getWeapons() {
		return weapons;
	}

	public List<RoomSquare> getRoomSquares() {
		return rSquares;
	}

	/**
	 * @return the board
	 */
	public Square[][] getBoard() {
		return board;
	}

	public void setSize(int x, int y) {
		int squareWidth = x / 29;
		int squareHeight = y / 29;

		for (int i = 0; i < 29; i++) {
			for (int j = 0; j < 29; j++) {
				board[i][j].setSize(squareWidth, squareHeight);
			}
		}
	}

	public void drawBoard(Graphics g) {
		for (int i = 0; i < 29; i++) {
			for (int j = 0; j < 29; j++) {
				board[i][j].draw(g);
			}
		}
	}

	public List<Threshhold> getThreshholds() {
		return threshholds;
	}

	/**
	 * Reads the text file to create the board, which is a 2d array of Square
	 * objects
	 */
	private void readAndShowBoard() {
		int horCount = 0;
		int vertCount = 0;

		try {
			Scanner scan = new Scanner(file);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				Scanner lineScan = new Scanner(line);

				while (lineScan.hasNext()) {
					String square = lineScan.next();

					char first = square.charAt(0);

					switch (first) {
					case 'w':
						this.board[vertCount][horCount] = new Wall(vertCount, horCount);
						System.out.print("w"+" ");
						break;
					case 's':
						this.board[vertCount][horCount] = new FreeMove(vertCount, horCount);
						System.out.print("s"+" ");
						break;
					case '-':
						this.board[vertCount][horCount] = new Null(vertCount, horCount);
						System.out.print("-"+" ");
						break;
					case 'R':
						String rName = lineScan.next();

						Room room = null;
						for (Room r : rooms) {
							if (r.getName().equalsIgnoreCase(rName)) {
								room = r;
								break;
							}
						}

						if (room == null) {
							room = new Room(rName);
							rooms.add(room);
						}

						RoomSquare rs = new RoomSquare(room, vertCount, horCount);
						this.board[vertCount][horCount] = rs;
						rSquares.add(rs);
						room.setRoomSquare(rs);
						System.out.print("R"+" ");
						break;
					case 'D':
						String rNameDoor = lineScan.next();
						Room rm = null;

						for (Room r : rooms) {
							if (r.getName().equalsIgnoreCase(rNameDoor)) {
								rm = r;
								break;
							}
						}

						if (rm == null) {
							rm = new Room(rNameDoor);
							rooms.add(rm);
						}
						this.board[vertCount][horCount] = new Door(rm, vertCount, horCount);
						System.out.print("D"+" ");
						break;
					case 'T':
						String prName = lineScan.next();

						Room pDoorRoom = null;
						for (Room r : rooms) {
							if (r.getName().equalsIgnoreCase(prName)) {
								pDoorRoom = r;
								break;
							}
						}

						if (pDoorRoom == null) {
							pDoorRoom = new Room(prName);
							rooms.add(pDoorRoom);
						}

						Threshhold pDoor = new Threshhold(pDoorRoom, vertCount, horCount);
						this.board[vertCount][horCount] = pDoor;
						threshholds.add(pDoor);
						System.out.print("T"+" ");
						break;
					case 'S':
						String name = lineScan.next() + " " + lineScan.next();

						Suspect c = new Suspect(name, vertCount, horCount);
						c.setPlayable(false);
						suspects.add(c);

						this.board[vertCount][horCount] = new StartSquare(vertCount, horCount);
						System.out.print("S"+" ");
						break;
					default:
						break;
					}

					horCount++;
				}
				vertCount++;
				horCount = 0;
				System.out.println("");
			}

		} catch (IOException e) {
			throw new Error(e);
		}

		createWeapons();
		setNulls();
		// distributeWeapons();
	}

	/**
	 * Creates the weapons and adds them to the list of weapons.
	 */
	private void createWeapons() {
		weapons.add(new Weapon(Weapons.Rope));
		weapons.add(new Weapon(Weapons.Candlestick));
		weapons.add(new Weapon(Weapons.Knife));
		weapons.add(new Weapon(Weapons.LeadPipe));
		weapons.add(new Weapon(Weapons.Revolver));
		weapons.add(new Weapon(Weapons.Poison));
	}

	/**
	 * Every RoomSquare has 8 NullSquares around it, which the players will be
	 * placed on if they enter the room.
	 */
	private void setNulls() {
		for (RoomSquare rs : rSquares) {
			Null ns1 = (Null) board[rs.getCol() - 1][rs.getRow() - 1];

			rs.setNull(ns1);
			ns1.setRoomSquare(rs);

			Null ns2 = (Null) board[rs.getCol() - 1][rs.getRow()];
			rs.setNull(ns2);
			ns2.setRoomSquare(rs);

			Null ns3 = (Null) board[rs.getCol() - 1][rs.getRow() + 1];
			rs.setNull(ns3);
			ns3.setRoomSquare(rs);

			Null ns4 = (Null) board[rs.getCol()][rs.getRow() - 1];
			rs.setNull(ns4);
			ns4.setRoomSquare(rs);

			Null ns5 = (Null) board[rs.getCol()][rs.getRow() + 1];
			rs.setNull(ns5);
			ns5.setRoomSquare(rs);

			Null ns6 = (Null) board[rs.getCol() + 1][rs.getRow() - 1];
			rs.setNull(ns6);
			ns6.setRoomSquare(rs);

			Null ns7 = (Null) board[rs.getCol() + 1][rs.getRow()];
			rs.setNull(ns7);
			ns7.setRoomSquare(rs);

			Null ns8 = (Null) board[rs.getCol() + 1][rs.getRow() + 1];
			rs.setNull(ns8);
			ns8.setRoomSquare(rs);
		}
	}

}
