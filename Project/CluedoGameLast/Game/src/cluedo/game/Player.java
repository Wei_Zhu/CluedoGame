package cluedo.game;

import java.util.ArrayList;
import java.util.List;
import cluedo.cards.Card;
import cluedo.elements.GameItem;
import cluedo.elements.NoteBook;
import cluedo.elements.Room;
import cluedo.elements.Suspect;
import cluedo.slots.RoomSquare;
import cluedo.slots.Square;

//import java.util.Scanner;

/**
 * 
 * @author zhuwe
 *
 */
public class Player {

	private Suspect sp;
	private Game game;
	private Board board;
	private int roll = 0;
	private boolean isPlaying;
	private Square on;
	private List<Card> cards;
	private NoteBook nb;
	private boolean current;
	private int pRow;
	private int pCol;
	private boolean pInRoom;
	private String roomName;

	/**
	 * Constructor for a player. They are given the board and the game, which makes
	 * sence as in real life a player would have access to both of these. It also
	 * makes this class much easier.
	 *
	 * @param sp
	 * @param board
	 * @param game
	 */
	public Player(Suspect sp, Board board, Game game) {
		this.sp = sp;
		this.board = board;
		this.game = game;
		isPlaying = false;
		current = false;
		cards = new ArrayList<Card>();
		nb = new NoteBook();
	}

	/**
	 * Returns a number between 2 and 12, simulating two six sided dice being
	 * rolled.
	 *
	 * @return
	 */
	public int roll() {
		roll = Board.dice.roll();
		return roll;
	}

	public void clearRoll() {
		roll = 0;
	}

	public Suspect getSuspect() {
		return sp;
	}

	public int getRow() {
		return pRow;
	}

	public void setRow(int row) {
		this.pRow = row;
	}

	public int getCol() {
		return pCol;
	}

	public void setCol(int column) {
		this.pCol = column;
	}

	public boolean getInRoom() {
		return pInRoom;
	}

	public void setInRoom(boolean b) {
		this.pInRoom = b;
	}

	public String getRoomName() {
		RoomSquare roomSquare = (RoomSquare) board.getSquare(pCol, pRow);

		return roomSquare.getRoom().getName();

	}

	/**
	 * Sets the square that the player is currently standing on.
	 *
	 * @param s
	 */
	public void setOn(Square s) {
		this.on = s;
		s.setPlayer(this);
	}

	/**
	 * If this player is being controlled by a user then isPlaying will return true;
	 *
	 * @return
	 */
	public boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean b) {
		this.isPlaying = b;
	}

	public void addCard(Card c) {
		cards.add(c);
	}

	public void updateNoteBook(GameItem gi, boolean b) {
		nb.addToNoteBook(gi, b);
	}

	public void updateNoteBook() {
		for (Card c : cards) {
			if (c == null) {
				continue;
			}
			nb.addToNoteBook(c.getItem(), true);
		}
	}

	public void setCurrent(boolean b) {
		this.current = b;
	}

	public boolean isCurrent() {
		return current;
	}

	// method for test

	public List<Card> getCard() {
		return cards;

	}

	// current = get current player
	// playMove = plays move
	// get current players updated position
	// Check if current position is inside room
	// if yes,
	// System.out.println Ask for a suggestion
	// Move suspect in the current player's room
	// while (loop through all players, starting from left):
	// Ask current player to reveal the suggested card of they have it.
	// if yes:
	// //not sure if you want to implement note-taking functionality
	// else:
	// break
	// Ask for accusation
	// If yes:
	// Ask for which player/room/weapon
	// If matches:
	// System.out(current player wins)
	// return (or break)
	// else:
	// Remove the current player from List
	// public void executeAccuse(Weapon w, Room r, GameCharacter gc) {

}
//
