package cluedo.system;

import java.util.Scanner;
import cluedo.game.Game;

public class ChoosePlayer {

	private Game game;
	private int numPlayers;

	private int charNum;
	
	private int count;
////Choose player allows us to select the number of player from 3 to 6 and refuses if the number of players is greater than 6 or less than three
	// Then the suspects get to choose what suspect they want to impersonate from the list of the suspects.
	public ChoosePlayer(Game g) {
		this.game = g;
		this.charNum = 1;
		this.count = 1;
		NumberOfPlayer();
		createDummyPlayers();
		//if we run testClass as a java application, we need to comment game.playGame() function;
		//if we run Game class as java application, don't comment game.playGame() function;
		game.playGame();

	}

	public void NumberOfPlayer() {
		System.out.println("please choose the number of player between 3 and 6: ");
		Scanner sc = new Scanner(System.in);
		int numberOfPlayer = sc.nextInt();
		if (numberOfPlayer <= 6 && numberOfPlayer >= 3) {
			numPlayers = numberOfPlayer;
		} else {
			System.out.println("invalid player number, try again: ");
			Scanner sc1 = new Scanner(System.in);
			numPlayers = sc1.nextInt();
		}
	}

	public void createDummyPlayers() {
		while (numPlayers >= count) {
			System.out.println("1.Mrs Peacock	2.Professor Plum  	3.Mr Green	4.Mrs White	5.Colonel Mustard	6.Miss Scarlett");
			System.out.println("for example, you want to play Mrs Peacork, type 1. choose one suspect you want to play: ");
			Scanner sc = new Scanner(System.in);
			charNum = sc.nextInt();
			game.createDummyPlayers(charNum);
			count++;
			System.out.println("------------------------------------------------------------");
		}
	}

}
