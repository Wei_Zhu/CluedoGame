package window;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import cluedo.game.Board;
import cluedo.game.Game;

public class BoardFrame extends JFrame {

	// Fields relating to the actual game.
	private Board board;
	private Game game;

	// Fields relating to the Gui
	private BoardPanel boardpanel;

	public BoardFrame(Board board, Game game) {
		super("Cluedo");
		
		getContentPane().setBackground(Color.BLACK);

		this.board = board;
		this.game = game;
		
		setSize(950, 840);
		setMinimumSize(new Dimension(950, 840));

		// For Centering frame in the middle of the screen.
		// ----------------------------------------------------------
		// Get the size of the screen
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		// Determine the new location of the window
		int w = this.getSize().width;
		int h = this.getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;

		// Move the window
		setLocation(x, y);
		// ----------------------------------------------------------

		// Creates the center panel which draws the board.
		boardpanel = new BoardPanel(592, 580, board, game);
		this.board.setSize(600, 600);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		pack();
	}

}
