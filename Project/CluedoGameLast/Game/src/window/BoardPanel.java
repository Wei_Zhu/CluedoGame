package window;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import cluedo.game.Board;
import cluedo.game.Game;

public class BoardPanel extends JPanel {

	private int width;
	private int height;
	private BufferedImage boardImage;
	private File boardFile;
	private Board board;
	private Game game;

	public BoardPanel(int width, int height, Board board, Game game) {
		this.width = width;
		this.height = height;

		this.board = board;
		this.game = game;

		boardFile = new File("src/Default.jpg");

		// Loads the image into the buffered image
		try {
			boardImage = ImageIO.read(boardFile);
		} catch (IOException e) {
			throw new Error(e);
		}

		setPreferredSize(new Dimension(width, height));
		setVisible(true);
	}
	
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    
	public void paintComponent(Graphics g) {
		g.drawImage(boardImage, 0, 0, width, height, null);
		board.drawBoard(g);
	}
	
    /**
     * @param boardFile the boardFile to set
     */
    public void setBoardFile(File boardFile) {
        this.boardFile = boardFile;

        try {
            boardImage = ImageIO.read(boardFile);
        } catch (IOException e) {
            throw new Error(e);
        }

        repaint();
    }

}
